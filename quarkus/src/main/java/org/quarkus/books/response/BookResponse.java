package org.quarkus.books.response;

import lombok.*;


@Builder
@Data
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class BookResponse {
    private String name;
    private String author;
}
