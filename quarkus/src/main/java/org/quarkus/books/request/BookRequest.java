package org.quarkus.books.request;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
public class BookRequest {
    private String name;
    private String author;
}
