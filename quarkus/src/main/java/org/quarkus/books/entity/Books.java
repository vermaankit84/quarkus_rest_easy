package org.quarkus.books.entity;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Getter
public class Books {
    private String id;
    private String name;
    private String author;
}
