package org.quarkus.books.service.impl;

import org.quarkus.books.entity.Books;
import org.quarkus.books.request.BookRequest;
import org.quarkus.books.response.BookResponse;
import org.quarkus.books.service.IBookService;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@ApplicationScoped
public class BookService implements IBookService {

    private final List<Books> booksList = new ArrayList<>();

    @PostConstruct
    public void init() {
        booksList.add(Books.builder().id(UUID.randomUUID().toString()).name("2019 How modi WOn India").author("Rajdeep Sardesi").build());
    }

    @Override
    public List<BookResponse> getAllBooks() {
        return booksList.stream().
                map(books -> BookResponse.builder().author(books.getAuthor()).name(books.getName()).build()).
                collect(Collectors.toList());
    }

    @Override
    public List<BookResponse> getByAuthor(String authorName) {
        return booksList.stream().
                filter(books -> authorName.equalsIgnoreCase(books.getAuthor())).
                map(books -> BookResponse.builder().author(books.getAuthor()).name(books.getName()).build()).
                collect(Collectors.toList());
    }

    @Override
    public BookResponse insert(BookRequest bookRequest) {
        booksList.add(Books.builder().id(UUID.randomUUID().toString()).author(bookRequest.getAuthor()).name(bookRequest.getName()).build());
        return BookResponse.builder().author(bookRequest.getAuthor()).name(bookRequest.getName()).build();
    }
}
