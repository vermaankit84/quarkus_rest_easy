package org.quarkus.books.service;


import org.quarkus.books.request.BookRequest;
import org.quarkus.books.response.BookResponse;

import java.util.List;

public interface IBookService {
    List<BookResponse> getAllBooks();

    List<BookResponse> getByAuthor(final String authorName);

    BookResponse insert(BookRequest bookRequest);
}
