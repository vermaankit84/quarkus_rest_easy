package org.quarkus.books.api;

import org.quarkus.books.request.BookRequest;
import org.quarkus.books.response.BookResponse;
import org.quarkus.books.service.IBookService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("books")
public class BookController {


    @Inject
    private IBookService bookService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<BookResponse> getAllBooks() {
        return bookService.getAllBooks();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{authorName}")
    public List<BookResponse> getByAuthor(@PathParam("authorName") final String authorName) {
        return bookService.getByAuthor(authorName);
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public BookResponse insert(final BookRequest bookRequest) {
        return bookService.insert(bookRequest);
    }
}
